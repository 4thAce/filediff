#!/usr/bin/env ruby

# Just use the shell command diff to check whether the current version of a
# file matches stored versions
class Diff2

  LOGDIR="/srv/log"
  require 'trollop'
  #require 'fog/aws'
  require 'net/scp'
  attr_reader :filelist
  attr_reader :username

  def initialize(passed_filelist, passed_user)
    @filelist=passed_filelist
    #@auth=passed_auth
    @username=passed_user
    if File.exists?("#{LOGDIR}/diff2.1.txt")
      if File.writable?("#{LOGDIR}/diff2.1.txt")
          File.delete("#{LOGDIR}/diff2.1.txt")
      else
        exit 127
      end
    end
  end

  def from_file_unchanged
    list=File.open(@filelist, 'r')
    # The file is in the format
    # hostname file_to_check saved_file
    # so here you will get for example
    # ['cron001','/etc/hosts','/srv/etc/last/cron001/etc/hosts']
    (hostname, remote, saved) = list.read.split(/\s+/)
    #puts "Hostname #{hostname} Remote #{remote} Local #{saved}"
    # Download the file from the target server
    rc = remote_unchanged(hostname,remote,saved)
    return rc
  end

  def remote_unchanged(remote_host,remote_path,local)
    if !File.exists? local
      return 0x40
    end
    outcome = 0
    dlfile='/srv/tmp/test'
    current = Net::SCP.start(remote_host, 'root', :keys => ['/root/.ssh/id_rsa']) do |scp|
      task = scp.download(remote_path, dlfile)
      task.wait
    end
    same_or_not = %x{diff #{dlfile} #{local} >> #{LOGDIR}/diff2.1.txt; echo $?}  # Let the OS do it
    if(same_or_not.to_i != 0)
      outcome = 0x10 | outcome
      outfile = File.open("#{LOGDIR}/diff2.1.txt",'w')
      outfile.puts "[#{@environment}] #{remote_path} at #{remote_host} has changed"
      outfile.close
    end
    return outcome
  end
end
