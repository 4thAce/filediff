# Constructs the list of autoscale instances and the files to be checked
# on each

class CurrentAsList

  require 'fog/aws'

  attr_reader :awspath
  attr_reader :username

  def initialize(passed_access, passed_sec, passed_region, passed_user)
    @sec=passed_sec
    @access=passed_access
    @region = passed_region
    @username=passed_user
  end

  def grabIPs(groupName)
    if @access.nil? || @sec.nil? || @region.nil?
      puts "Incomplete AWS information"
      exit -1
    end
    # Construct a hash of local IP addresses keyed to instance IDs
    ec2hash=Hash.new
    ec2=Fog::Compute.new :provider => 'AWS',
                     :region => @region,
                     :aws_access_key_id => @access,
                     :aws_secret_access_key => @sec
    ec2.describe_instances.body['reservationSet'].each do |instance|
      isobject = instance['instancesSet'].each do |anobject|
        ec2hash[anobject['instanceId']]=anobject['privateIpAddress']
      end
    end
    asary=Array.new
    as=Fog::AWS::AutoScaling.new :region => @region,
                     :aws_access_key_id => @access,
                     :aws_secret_access_key => @sec
    as.describe_auto_scaling_instances.body['DescribeAutoScalingInstancesResult']['AutoScalingInstances'].each do |instance|
       id=instance['InstanceId']
       group=instance['AutoScalingGroupName']
       if group == groupName
         asary.push(ec2hash[id])
       end
    end
    # Now return the array
    return asary
  end

end
