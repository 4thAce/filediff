require_relative "./spec_helper"

describe CurrentAsList do

    it "constructs successfully" do
      aaa = CurrentAsList.new('/Users/richardmagahiz/Development/filediff/spec/aws_spec','rmagahiz')
      expect(aaa).to be_a(CurrentAsList)
    end

  it "returns an array of length 3" do
    aslist = CurrentAsList.new('/Users/richardmagahiz/Development/filediff/spec/aws_spec','rmagahiz')
    count = aslist.grabIPs('dsweb').length
    expect(count).to eq(3)
  end

  it "returns an array of quad octets" do
    aslist = CurrentAsList.new('/Users/richardmagahiz/Development/filediff/spec/aws_spec','rmagahiz')
    addresses = aslist.grabIPs('dsweb')
    addresses.each { |privateIP|
      expect(privateIP).to match(/^(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2})(\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[0-9]{1,2})){3}$/)
    }
  end

end
