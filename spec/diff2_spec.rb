require_relative "./spec_helper"

describe Diff2 do
    #PATH=%x{pwd}
    #puts "Path #{PATH}"

    it "constructs successfully" do
    aaa = Diff2.new('/Users/richardmagahiz/Development/filediff/spec/filelist_spec','/Users/richardmagahiz/Development/filediff/spec/aws_spec','rmagahiz')
    expect(aaa).to be_a(Diff2)
  end

  it "passes when comparing a local file with itself" do
    aaa = Diff2.new('/Users/richardmagahiz/Development/filediff/spec/filelist_spec','/Users/richardmagahiz/Development/filediff/spec/aws_spec','rmagahiz')
    #selfcheck=aaa.check_remote('54.157.157.223',"/home/rmagahiz/s1","#{PATH}/s1")
    selfcheck=aaa.remote_unchanged('54.157.157.223',"s1","/Users/richardmagahiz/Development/filediff/spec/s1")
    expect(selfcheck).to eq(true)
  end

  it "fails when comparing a local file with null" do
    aaa = Diff2.new('/Users/richardmagahiz/Development/filediff/spec/filelist_spec','/Users/richardmagahiz/Development/filediff/spec/aws_spec','rmagahiz')
    selfcheck=aaa.remote_unchanged('54.157.157.223',"s1",'/dev/null')
    expect(selfcheck).to eq(false)
  end

  it "can read in an aws credential file" do
    aaa = Diff2.new('/Users/richardmagahiz/Development/filediff/spec/filelist_spec','/Users/richardmagahiz/Development/filediff/spec/aws_spec','rmagahiz')
    awsread=aaa.get_awscred
    expect(awsread).to eq(true)
  end

  it "reads in info from a file okay" do
    aaa = Diff2.new('/Users/richardmagahiz/Development/filediff/spec/filelist_spec','/Users/richardmagahiz/Development/filediff/spec/aws_spec','rmagahiz')
    remotecheck=aaa.from_file_unchanged
    expect(remotecheck).to eq(true)
  end

end
