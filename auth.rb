#!/usr/bin/env ruby
# Just read the aws cred file once

class Auth
  attr_reader :sec
  attr_reader :access
  attr_reader :region
  attr_reader :environment

  def initialize(awspath)
    @awspath=awspath
    unless get_awscred
      puts "Error reading AWS file"
      exit -1
    end
  end

  def get_awscred
      File.open(@awspath,'r') do |cred|
        tokens=[]
        cred.each { |line|
          linesplit=line.split(/\s+/)    # ['AWS_SECRET_ACCESS_KEY','d8685fc2a1bbc2164ae4fddfe1cde776/b52f67ce194c79471a22bfc']
          next if linesplit.length < 2
          tokens=linesplit[1].split(/=/)
          case tokens[0]
          when 'AWS_SECRET_ACCESS_KEY'
            @sec = tokens[1].chomp
          when 'AWS_ACCESS_KEY_ID'
            @access = tokens[1].chomp
          when 'AWS_DEFAULT_REGION'
            @region = tokens[1].chomp
          when 'ENVIRONMENT'
            @environment = tokens[1].chomp
          end
        }
      end
      allread = !(@sec.nil? || @access.nil? || @region.nil? || @environment.nil?)
      return allread
  end

end
